package test_swing;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.regex.Pattern;

public class TextFieldDemo {

	JLabel labelFild, labelFild2;
	JTextField textField, textField2;
	private final static Pattern DIGITS = Pattern.compile("\\d+");
	private final static Pattern LETTERS = Pattern.compile("[A-Za-z]+");

	TextFieldDemo() {
		JFrame frame = new JFrame("JTextFieldExample");
		frame.getContentPane().setLayout(new FlowLayout());
		frame.setSize(500, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		textField = new JTextField(25);
		labelFild = new JLabel("Write only numbers ...");

		textField2 = new JTextField(25);
		labelFild2 = new JLabel("Write only letters ...");

		textField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (DIGITS.matcher(textField.getText()).matches()) {
					JOptionPane.showMessageDialog(null, "OK! You entered correctly", "INFORMATION",
							JOptionPane.INFORMATION_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(null, "Error. You entered not numbers!", "Uh-oh!",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		textField2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (LETTERS.matcher(textField2.getText()).matches()) {
					JOptionPane.showMessageDialog(null, "OK! You entered correctly", "INFORMATION",
							JOptionPane.INFORMATION_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(null, "Error. You entered not letters!", "Uh-oh!",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		frame.add(textField);
		frame.add(labelFild);
		frame.add(textField2);
		frame.add(labelFild2);

		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new TextFieldDemo();
			}
		});
	}
}
