package xml_feature;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import ua.xml.model.WellList;
import ua.xml.model.Wells;

public class JDomReadXMLFile {
	public static void main(String[] args) {

		WellList wellList = new WellList();

		SAXBuilder builder = new SAXBuilder();

		try {
			Document document = (Document) builder.build(new File("d:/wellsData.xml"));
			Element root = document.getRootElement();

			List<Wells> wellsList = new ArrayList<>();
			List<Element> wellsListRoot = root.getChildren();
			List<Element> wellsListChildren = new ArrayList<>();

			for (Element wellElement : wellsListRoot) {

				wellsListChildren = wellElement.getChildren();

				for (Element element : wellsListChildren) {

					Wells wells = new Wells();
					wells.setWellName(element.getAttributeValue("WellName"));
					wells.setWellID(Integer.parseInt(element.getChildText("WellID")));
					wells.setWellCode(element.getChildText("WellCode"));
					wells.setSectionType(element.getChildText("SectionType"));
					wells.setDatum(element.getChildText("Datum"));

					wellsList.add(wells);
				}
				wellList.setWells(wellsList);
			}
			wellList.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
