package xml_feature;

public class Wells {

	private String WellName, WellCode, SectionType, Datum;
	private int WellID;

	public String getWellName() {
		return WellName;
	}

	public void setWellName(String wellName) {
		WellName = wellName;
	}

	public String getWellCode() {
		return WellCode;
	}

	public void setWellCode(String wellCode) {
		WellCode = wellCode;
	}

	public String getSectionType() {
		return SectionType;
	}

	public void setSectionType(String sectionType) {
		SectionType = sectionType;
	}

	public String getDatum() {
		return Datum;
	}

	public void setDatum(String datum) {
		Datum = datum;
	}

	public int getWellID() {
		return WellID;
	}

	public void setWellID(int wellID) {
		WellID = wellID;
	}
	


	@Override
	public String toString() {
		
		System.out.printf("	%-12s		%-12s	%-12s		%-12s		%-21s%n", WellName, WellID, WellCode, SectionType,Datum);
		
		return "";
	}

}
