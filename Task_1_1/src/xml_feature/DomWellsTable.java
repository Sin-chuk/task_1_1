package xml_feature;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class DomWellsTable {

	public static void main(String[] args) {

		WellList wellList = new WellList();

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		try {
			builder = factory.newDocumentBuilder();
			Document document = builder.parse(new File("d:/wellsData.xml"));
			Node rootNode = document.getFirstChild();
			Node wellsNode = null;

			NodeList rootchild = rootNode.getChildNodes();
			for (int i = 0; i < rootchild.getLength(); i++) {

				if (rootchild.item(i).getNodeType() != Node.ELEMENT_NODE)
					continue;

				switch (rootchild.item(i).getNodeName()) {

				case "wells":
					wellsNode = rootchild.item(i);
					break;
				}
			}

			if (wellsNode == null) {
				return;
			}

			List<Wells> wellsList = new ArrayList<>();
			NodeList childWellsNode = wellsNode.getChildNodes();

			for (int j = 0; j < childWellsNode.getLength(); j++) {

				if (childWellsNode.item(j).getNodeType() != Node.ELEMENT_NODE)
					continue;

				if (!childWellsNode.item(j).getNodeName().equals("Well")) {
					continue;
				}

				NodeList elementWell = childWellsNode.item(j).getChildNodes();
				Element element;
				Wells wells = new Wells();

				element = (Element) childWellsNode.item(j);
				wells.setWellName(element.getAttribute("WellName").toString());

				for (int k = 0; k < elementWell.getLength(); k++) {

					if (elementWell.item(k).getNodeType() != Node.ELEMENT_NODE)
						continue;

					switch (elementWell.item(k).getNodeName()) {

					case "WellID":
						wells.setWellID(Integer.valueOf(elementWell.item(k).getTextContent()));
						break;

					case "WellCode":
						wells.setWellCode(elementWell.item(k).getTextContent());
						break;

					case "SectionType":
						wells.setSectionType(elementWell.item(k).getTextContent());
						break;

					case "Datum":
						wells.setDatum(elementWell.item(k).getTextContent());
						break;
					}
				}
 
				wellsList.add(wells);
				wellList.setWells(wellsList);
			}

			wellList.show();

		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
	}
}
