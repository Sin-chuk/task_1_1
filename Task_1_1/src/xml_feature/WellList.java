package xml_feature;

import java.util.List;

public class WellList {

	private List<Wells> wells;

	public List<Wells> getWells() {
		return wells;
	}

	public void setWells(List<Wells> wells) {
		this.wells = wells;
	}

	public void show() {

		System.out.println();
		System.out.printf("	%-12s	%12s		%-12s		%-12s		%-21s%n", "WellName", "WellID",
				"WellIDWellCode", "SectionType", "Datum");

		for (Wells well : wells) {
			System.out.println(
					"-------------------------------------------------------------------------------------------------------------");
			System.out.println(well);
		}
	}
}
