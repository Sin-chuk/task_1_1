package ua.task1_1;

import java.io.IOException;

public class ScannerParam1_1 {
	public static void main(String[] args) {

		ServerParametersI serverParametersStoreImpl = ServerParametersStoreImpl.getInstance(args);
		System.out.println();
		EndPointReaderImpl endPointReader = new EndPointReaderImpl(serverParametersStoreImpl);
		try {
			System.out.println(endPointReader.readEndPoint());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
