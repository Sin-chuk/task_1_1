package ua.task1_1;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;
import java.util.logging.Level;

public class EndPointReaderImpl {

	private URL url;
	private String urlName;
	private String encoding;
	private URLConnection urlConnection;
	private ServerParametersI serverParametersI;
	private StringBuilder stringBuilder = new StringBuilder(50);

	EndPointReaderImpl(ServerParametersI serverParametersI) {
		this.serverParametersI = serverParametersI;
	}

	public String readEndPoint() throws IOException {

		urlName = serverParametersI.getCommandValue("server");
		url = new URL(urlName);
		urlConnection = url.openConnection();
		urlConnection.connect();
		ServerParametersStoreImpl.LOGGER.log(Level.INFO,
				"Connection to " + urlName + " has been established successfully.");
		encoding = urlConnection.getContentEncoding();
		if (encoding == null)
			encoding = "UTF-8";
		Scanner scanner = new Scanner(urlConnection.getInputStream(), encoding);
		/**
		 * displays specified number of lines of the transmitted URL
		 */
		for (int j = 1; scanner.hasNextLine() && j <= 5; j++) {
			stringBuilder.append(scanner.nextLine());
		}
		scanner.close();
		if (stringBuilder.toString() != null)
			ServerParametersStoreImpl.LOGGER.log(Level.INFO, "Data from " + urlName + " received successfully.");
		else
			ServerParametersStoreImpl.LOGGER.log(Level.WARNING, "Data from " + urlName + " NOT received.");

		return stringBuilder.toString();
	}
}
