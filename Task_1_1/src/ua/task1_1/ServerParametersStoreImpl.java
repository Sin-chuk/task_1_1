package ua.task1_1;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServerParametersStoreImpl implements ServerParametersI {

	private static final String DELIMETER = "=";
	private static ServerParametersStoreImpl instance;
	static Logger LOGGER;
	private Map<String, String> parametersStore = new HashMap<>();
	private String[] tempParameters = new String[2];
	private String[] errorString = { "ERROR ", "PARAMETER" };
	private String commandValue;

	static {
		System.setProperty("java.util.logging.config.file", "d:/logging.properties");
		LOGGER = Logger.getLogger(ServerParametersStoreImpl.class.getName());
		LOGGER.setLevel(Level.FINEST);
	}

	public static ServerParametersStoreImpl getInstance(String[] parametersArgs) {
		if (instance == null) {
			instance = new ServerParametersStoreImpl(parametersArgs);
			LOGGER.log(Level.INFO, "Creating a single instance of a class.");
		}
		LOGGER.log(Level.WARNING, "Returning of a single instance of the class.");
		return instance;
	}

	private ServerParametersStoreImpl(String[] parametersArgs) {

		parsParameters(parametersArgs);
	}

	@Override
	public void parsParameters(String[] parameterArgs) {
		/**
		 * saving command line parameters to map
		 */
		for (int k = 0; k < parameterArgs.length; k++) {

			tempParameters = parameterArgs[k].split(DELIMETER);
			parametersStore.put(tempParameters[0], tempParameters[1]);
		}
		LOGGER.log(Level.FINE, "Command line parameters are saved in map.");
	}

	@Override
	public String[] getParameterValueAndCommandValue(String parameter) {

		for (Entry<String, String> e : parametersStore.entrySet()) {

			if (parameter.equalsIgnoreCase(e.getKey())) {
				tempParameters[0] = e.getKey();
				tempParameters[1] = e.getValue();
				LOGGER.log(Level.FINE, "Parameter and command returned successfully.");
				break;
			} else {
				LOGGER.log(Level.WARNING, "Parameter value and command value returned with an error.");
				return errorString;
			}
		}
		return tempParameters;
	}

	@Override
	public String getCommandValue(String parameter) {

		for (Entry<String, String> e : parametersStore.entrySet()) {

			if (parameter.equalsIgnoreCase(e.getKey())) {
				commandValue = e.getValue();
				LOGGER.log(Level.INFO, "Command value returned successfully.");
				break;
			} else {
				LOGGER.log(Level.WARNING, "Command value not found.");
				return "Command value not found.";
			}

		}
		return commandValue;
	}
}
