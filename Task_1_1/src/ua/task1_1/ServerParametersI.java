package ua.task1_1;

/**
 * contains methods for processing input parameters from the command line
 * 
 * @author 38050
 *
 */

public interface ServerParametersI {
	/**
	 * Parameter name constants
	 */
	static final String SERVER = "server";
	static final String FORMAT = "format";
	static final String PASSWORD = "password";
	static final String USER = "user";
	static final String ENDPOINT = "endpoint";

	/**
	 * returns command by parameter
	 * 
	 * @param parameterValue
	 * @return
	 */
	String getCommandValue(String parameterValue);

	/**
	 * returns a pair of parameter and command by input parameter
	 * 
	 * @param parameterValue
	 * @return
	 */
	String[] getParameterValueAndCommandValue(String parameterValue);

	/**
	 * parsing parameters
	 * 
	 * @param parameterArgs
	 */
	void parsParameters(String[] parameterArgs);
}
