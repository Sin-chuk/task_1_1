package test_swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;

public class ComboboxExample {

	public static void main(String[] args) {

		Display display = new Display();
		Shell shell = new Shell(display);

		shell.setSize(500, 200);
		Monitor primary = display.getPrimaryMonitor();
		Rectangle bounds = primary.getBounds();
		Rectangle rect = shell.getBounds();
		int x = bounds.x + (bounds.width - rect.width) / 2;
		int y = bounds.y + (bounds.height - rect.height) / 2;
		shell.setLocation(x, y);
		
        RowLayout rowLayout = new RowLayout();
        rowLayout.marginLeft = 10;
        rowLayout.marginTop = 10;
        shell.setLayout(rowLayout);
        
        Label label = new Label(shell, SWT.NONE);
        label.setText("Select language:");
        
		Combo combo = new Combo(shell, SWT.DROP_DOWN);
		combo.setLayoutData(new RowData(150, 100));
		String[] comboString = { "English", "Vietnamese", "Russian" };
		combo.setItems(comboString);
		Label labelMessage = new Label(shell, SWT.NONE);

        combo.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                int idx = combo.getSelectionIndex();
                String language = combo.getItem(idx);
                labelMessage.setText("Select: " + idx + " - " + language);
                labelMessage.pack();
            }
        });
		
        combo.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                // User pressed Enter.
                if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) {
                    String text = combo.getText();
                    if (text == null || text.length() == 0) {
                        return;
                    }
                    String[] items = combo.getItems();
                    for (String item : items) {
                        if (item.equals(text)) {
                            return;
                        }
                    }
                    combo.add(text);
                }
            }
        });
		
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}
}
