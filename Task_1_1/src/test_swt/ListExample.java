package test_swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;

public class ListExample {

	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setSize(430, 200);
		shell.setText("SWT List");
		Monitor primary = display.getPrimaryMonitor();
		Rectangle bounds = primary.getBounds();
		Rectangle rect = shell.getBounds();
		int x = bounds.x + (bounds.width - rect.width) / 2;
		int y = bounds.y + (bounds.height - rect.height) / 2;
		shell.setLocation(x, y);

		RowLayout layout = new RowLayout(SWT.VERTICAL);
		layout.spacing = 10;
		layout.marginHeight = 10;
		layout.marginWidth = 10;

		shell.setLayout(layout);

		// Create a List
		// (Allows selecte multiple lines and display vertical scroll bar.).
		final List list = new List(shell, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		list.setLayoutData(new RowData(300, 100));

		list.add("Apple");
		list.add("Apricot");
		list.add("Banana");
		list.add("Carrot");
		list.add("Cherry");
		list.add("Courgette");
		list.add("Endive");
		list.add("Grape");

		Label label = new Label(shell, SWT.NONE);
		label.setLayoutData(new RowData(100, SWT.DEFAULT));

		list.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent event) {
				int[] selections = list.getSelectionIndices();
				String outText = "";
				for (int i = 0; i < selections.length; i++) {
					outText += selections[i] + " ";
				}
				label.setText("You selected: " + outText);
			}
		});

		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		display.dispose();
	}
}
