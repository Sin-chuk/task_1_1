package test_swt;

import org.eclipse.swt.*;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.*;

public class MenuExample {

	public static void main(String[] args) {

		Display display = new Display();
		Shell shell = new Shell(display);

		shell.setSize(500, 200);
		Monitor primary = display.getPrimaryMonitor();
		Rectangle bounds  = primary.getBounds();
		Rectangle rect = shell.getBounds();
		int x = bounds.x + (bounds.width - rect.width) / 2;
		int y = bounds.y + (bounds.height - rect.height) / 2;
	    shell.setLocation (x, y);

		Menu menuBar = new Menu(shell, SWT.BAR);
		shell.setMenuBar(menuBar);

		MenuItem fileItem = new MenuItem(menuBar, SWT.CASCADE);
		fileItem.setText("File");

		MenuItem editItem = new MenuItem(menuBar, SWT.CASCADE);
		editItem.setText("Edit");

		Menu submenuFile = new Menu(shell, SWT.DROP_DOWN);
		fileItem.setMenu(submenuFile);
		
//		MenuItem menuItem = new MenuItem(menuBar, SWT.SEPARATOR);
		MenuItem itemFile1 = new MenuItem(submenuFile, SWT.PUSH);
		itemFile1.setText("Select All\tCtrl+A");
		itemFile1.setAccelerator(SWT.MOD1 + 'A');
		itemFile1.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event e) {
				System.out.println("Select All");
			}
		});
		MenuItem itemFile2 = new MenuItem(submenuFile, SWT.PUSH);
		itemFile2.setText("Exit\tCtrl+Q");
		itemFile2.setAccelerator(SWT.MOD1 + 'Q');
		itemFile2.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event e) {
				System.exit(0);
			}
		});
		
		Menu submenuEdit = new Menu(shell, SWT.DROP_DOWN);
		editItem.setMenu(submenuEdit);
		MenuItem itemEdit1 = new MenuItem(submenuEdit, SWT.PUSH);
		itemEdit1.setText("Edit file\tCtrl+T");
		itemEdit1.setAccelerator(SWT.MOD1 + 'T');
		itemEdit1.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event arg0) {
				System.out.println("Edit file");
			}
		});
		
//		shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}

		}
	}

}
