package test_swt.layouts;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class FormLayoutExample {

	private static void createButtons(Shell parent) {
		Button btnOk = new Button(parent, SWT.PUSH);
		btnOk.setText("OK");

		FormData fdOk = new FormData(60, 24);
		fdOk.right = new FormAttachment(95, -70);
		fdOk.bottom = new FormAttachment(100, -5);
		btnOk.setLayoutData(fdOk);

		Button btnCancel = new Button(parent, SWT.PUSH);
		btnCancel.setText("������");

		FormData fdCancel = new FormData(60, 24);
		fdCancel.right = new FormAttachment(100, -10);
		fdCancel.bottom = new FormAttachment(100, -5);
		btnCancel.setLayoutData(fdCancel);
	}

	private static void createGUI(Shell parent) {

		Label lblLogin = new Label(parent, SWT.NONE);
		lblLogin.setText("�����");

		FormData formData = new FormData(60, 20);
		formData.top = new FormAttachment(0, 10);
		formData.left = new FormAttachment(0, 10);
		lblLogin.setLayoutData(formData);

		Text txtLogin = new Text(parent, SWT.BORDER);

		formData = new FormData(SWT.DEFAULT, SWT.DEFAULT);
		formData.top = new FormAttachment(0, 10);
		formData.left = new FormAttachment(lblLogin, 10);
		formData.right = new FormAttachment(100, -10);
		formData.height = 16;
		txtLogin.setLayoutData(formData);

		Label password = new Label(parent, SWT.NONE);
		password.setText("Password");
		formData = new FormData(60, 20);
		formData.top = new FormAttachment(lblLogin, 10);
		formData.left = new FormAttachment(0, 10);
		password.setLayoutData(formData);

		Text passwordLogin = new Text(parent, SWT.BORDER);
		formData = new FormData(SWT.DEFAULT, SWT.DEFAULT);
		formData.top = new FormAttachment(txtLogin, 10);
		formData.left = new FormAttachment(password, 10);
		formData.right = new FormAttachment(100, -10);
		formData.height = 16;
		passwordLogin.setLayoutData(formData);

		createButtons(parent);
	}

	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);

		shell.setText("������ FormLayout");

		shell.setSize(200, 200);
		Monitor primary = display.getPrimaryMonitor();
		Rectangle bounds = primary.getBounds();
		Rectangle rect = shell.getBounds();
		int x = bounds.x + (bounds.width - rect.width) / 2;
		int y = bounds.y + (bounds.height - rect.height) / 2;
		shell.setLocation(x, y);

		shell.setLayout(new FormLayout());

		createGUI(shell);

		shell.setSize(280, 180);
		shell.open();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		display.dispose();
	}
}
