package test_swt.layouts;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;

public class RowLayoutExsample {

	public static void main(String[] args) {

		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setText("RowLayout");
		
		shell.setSize(600, 300);
		Monitor primary = display.getPrimaryMonitor();
		Rectangle bounds  = primary.getBounds();
		Rectangle rect = shell.getBounds();
		int x = bounds.x + (bounds.width - rect.width) / 2;
		int y = bounds.y + (bounds.height - rect.height) / 2;
	    shell.setLocation (x, y);
		
		RowLayout rowLayout = new RowLayout();
		rowLayout.type = SWT.HORIZONTAL;
		rowLayout.marginTop = 20;
		rowLayout.marginBottom = 20;
		rowLayout.marginLeft = 20;
		rowLayout.marginRight = 20;
		rowLayout.spacing = 50;
		rowLayout.wrap = false;
		rowLayout.pack = true;
		rowLayout.justify = true;
		shell.setLayout(rowLayout);

		Button button1 = new Button(shell, SWT.PUSH);
		button1.setText("button1");
		button1.setLayoutData(new RowData(180, 30));

		Button button2 = new Button(shell, SWT.PUSH);
		button2.setText("button2");
		button2.setLayoutData(new RowData(150, 150));

//		shell.pack();
		shell.open();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		display.dispose();

	}

}
