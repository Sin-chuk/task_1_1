package test_swt.layouts;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;

public class FillLayoutExample {

	public static void main(String[] args) {

		Display display = new Display();
		final Shell shell = new Shell(display);
		shell.setText("FillLayout");
		
		shell.setSize(600, 200);
		Monitor primary = display.getPrimaryMonitor();
		Rectangle bounds  = primary.getBounds();
		Rectangle rect = shell.getBounds();
		int x = bounds.x + (bounds.width - rect.width) / 2;
		int y = bounds.y + (bounds.height - rect.height) / 2;
	    shell.setLocation (x, y);
		
		FillLayout fillLayout = new FillLayout();
        fillLayout.type = SWT.HORIZONTAL;
		fillLayout.marginWidth = 50;
		fillLayout.marginHeight = 50;
		fillLayout.spacing = 20;
		shell.setLayout(fillLayout);

		new Button(shell, SWT.PUSH).setText("Bitton1");
		new Button(shell, SWT.PUSH).setText("Bitton2");
		new Button(shell, SWT.PUSH).setText("Bitton3");

//		shell.pack();
		shell.open();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		display.dispose();

	}

}
