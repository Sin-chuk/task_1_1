package test_swt.layouts;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;

public class GridLayoutExample {

	public static void main(String[] args) {
 
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setSize (400, 300);
		shell.setText("GridLayuot");
		
		Monitor primary = display.getPrimaryMonitor();
		Rectangle bounds  = primary.getBounds();
		Rectangle rect = shell.getBounds();
		int x = bounds.x + (bounds.width - rect.width) / 2;
		int y = bounds.y + (bounds.height - rect.height) / 2;
	    shell.setLocation (x, y);
		
		GridLayout gridLayout = new GridLayout(0, false);
		
        gridLayout.marginLeft  = 10;
        gridLayout.marginRight = 5;
        gridLayout.numColumns  = 3;
        gridLayout.makeColumnsEqualWidth = true;
        shell.setLayout(gridLayout);
        new Button(shell, SWT.PUSH).setText("button1");
        
        GridData gridData = new GridData(GridData.FILL, GridData.FILL, true, false);
        gridData.horizontalSpan = 2;
        gridData.verticalSpan = 2;
        Button button2 = new Button(shell, SWT.PUSH);
        button2.setText("button2");
        button2.setLayoutData(gridData);
   
        new Button(shell, SWT.PUSH).setText("button3");
        
        GridData gridData4 = new GridData(GridData.FILL, GridData.FILL, true, false);
        gridData4.horizontalSpan = 3;
        Button button4 = new Button(shell, SWT.PUSH);
        button4.setText("button4");
        button4.setLayoutData(gridData4);
        
        shell.setLayout(gridLayout);

//		shell.pack();
		shell.open();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		display.dispose();

	}

}
