package test_swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.*;

public class PushButtonExample {
	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);

		shell.setSize(700, 200);
		Monitor primary = display.getPrimaryMonitor();
		Rectangle bounds = primary.getBounds();
		Rectangle rect = shell.getBounds();
		int x = bounds.x + (bounds.width - rect.width) / 2;
		int y = bounds.y + (bounds.height - rect.height) / 2;
		shell.setLocation(x, y);

		FillLayout fillLayout = new FillLayout();
		fillLayout.marginWidth = 50;
		fillLayout.marginHeight = 50;
		fillLayout.spacing = 20;
		shell.setLayout(fillLayout);

		Button button = new Button(shell, SWT.CAP_FLAT);
		button.setText("button Push");
		button.pack();
		button.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				MessageBox messageBox = new MessageBox(shell);
				messageBox.setMessage("Button pressed");
				messageBox.open();
			}
		});

		Button buttonR1 = new Button(shell, SWT.RADIO);
		buttonR1.setText("radio button R1");
		buttonR1.pack();
		buttonR1.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				MessageBox messageBox = new MessageBox(shell);
				Button button = (Button) e.getSource();

				if (button.getSelection()) {
					messageBox.setMessage("check box " + button.getText() + " checked ");
					messageBox.open();
				}

				else {
					messageBox.setMessage("check box " + button.getText() + " unchecked ");
					messageBox.open();
				}
			}
		});
		
		Button buttonR3 = new Button(shell, SWT.RADIO);
		buttonR3.setText("radio button R3");
		buttonR3.pack();
		buttonR3.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				MessageBox messageBox = new MessageBox(shell);
				Button button = (Button) e.getSource();

				if (button.getSelection()) {
					messageBox.setMessage("check box " + button.getText() + " checked ");
					messageBox.open();
				}

				else {
					messageBox.setMessage("check box " + button.getText() + " unchecked ");
					messageBox.open();
				}
			}
		});

		Button buttonCB1 = new Button(shell, SWT.CHECK);
		buttonCB1.setText("check box1");
		buttonCB1.pack();
		buttonCB1.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				MessageBox messageBox = new MessageBox(shell);
				Button button = (Button) e.getSource();

				if (button.getSelection()) {
					messageBox.setMessage("check box " + button.getText() + " checked ");
					messageBox.open();
				}

				else {
					messageBox.setMessage("check box " + button.getText() + " unchecked ");
					messageBox.open();
				}

			}
		});
//    shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
//    shell.pack();
//    shell.open();
//    while (!shell.isDisposed()) {
//      if (!display.readAndDispatch()) {
//        display.sleep();
//      }
//    }
//    display.dispose();
	}
}