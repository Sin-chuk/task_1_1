package test_swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.*;

public class TableExample {

	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setSize(400, 400);
		shell.setText("Table Example");
		Monitor primary = display.getPrimaryMonitor();
		Rectangle bounds = primary.getBounds();
		Rectangle rect = shell.getBounds();
		int x = bounds.x + (bounds.width - rect.width) / 2;
		int y = bounds.y + (bounds.height - rect.height) / 2;
		shell.setLocation(x, y);

		final Text text = new Text(shell, SWT.BORDER);
		text.setBounds(25, 240, 220, 25);

		Table table = new Table(shell, SWT.CHECK | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
		table.setHeaderVisible(true);
		String[] titles = { "Col 1", "Col 2", "Col 3", "Col 4", "Col 5" };

		for (int loopIndex = 0; loopIndex < titles.length; loopIndex++) {
			TableColumn column = new TableColumn(table, SWT.NULL);
			column.setText(titles[loopIndex]);
		}

		for (int loopIndex = 0; loopIndex < 5; loopIndex++) {
			TableItem item = new TableItem(table, SWT.NULL);
			item.setText("Item " + loopIndex);
			item.setText(0, "Item " + loopIndex);
			item.setText(1, "Yes");
			item.setText(2, "No");
			item.setText(3, "A table item");
			switch (loopIndex) {
			case 0:
				item.setBackground(4, new Color(255, 0, 0));
				break;
			case 1:
				item.setBackground(4, new Color(0, 20, 255));
				break;
			case 2:
				item.setBackground(4, new Color(100, 0, 255));
				break;
			case 3:
				item.setBackground(4, new Color(100, 255, 0));
				break;
			case 4:
				item.setBackground(4, new Color(0, 100, 0));
				break;
				
			}
		}

		for (int loopIndex = 0; loopIndex < titles.length; loopIndex++) {
			table.getColumn(loopIndex).pack();
		}

		table.setBounds(25, 25, 300, 200);

		table.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				if (event.detail == SWT.CHECK) {
					text.setText("You checked " + event.item);
				} else {
					text.setText("You selected " + event.item);
				}
			}
		});

		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		display.dispose();

	}

}
