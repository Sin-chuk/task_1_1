package test_swt;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;

import org.eclipse.swt.*;

public class LableExample {

	public static void main(String[] args) {

		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setText("Labels");
		shell.setSize(500, 200);
		Monitor primary = display.getPrimaryMonitor();
		Rectangle bounds = primary.getBounds();
		Rectangle rect = shell.getBounds();
		int x = bounds.x + (bounds.width - rect.width) / 2;
		int y = bounds.y + (bounds.height - rect.height) / 2;
		shell.setLocation(x, y);  

		FillLayout fillLayout = new FillLayout();
		fillLayout.marginWidth = 50;
		fillLayout.marginHeight = 60;
		fillLayout.spacing = 10;
		shell.setLayout(fillLayout);

		Label label1 = new Label(shell, SWT.BORDER | SWT.CENTER);
		Font font = new Font(label1.getDisplay(), new FontData("Mono", 13, SWT.BOLD));
		label1.setFont(font);
		label1.setText("label 1");
		label1.setBackground(new Color(200, 0, 0));
		label1.setForeground(new Color(0, 0, 200));

		Label label2 = new Label(shell, SWT.ARROW_UP | SWT.COLOR_BLUE);
		label2.setText("label 2");

		Label label3 = new Label(shell, SWT.NONE);
		label3.setText("label 3");

		Label label4 = new Label(shell, SWT.NONE);
		label4.setText("label 4");

		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}

	}

}
